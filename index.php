<?php
$lang = isset($_COOKIE['lang']) ? $_COOKIE['lang'] : substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
switch ($lang) {
    case "ua":

        break;
    case "ru":
        header('Location: ./ru');
        break;
    default:
        header('Location: ./en');
        break;
}
?>
<!DOCTYPE html>
<html lang="ua">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Оксамитовий сезон у Bukovel</title>
    <meta property="og:type" content="website">
    <meta property="og:url" content="/">
    <meta name="twitter:url" content="/">
    <meta name="og:title" content="оксамитовий сезон у Bukovel">
    <meta name="og:image" content="img/sharing-picture.png">
    <meta name="twitter:title" content="оксамитовий сезон у Bukovel">
    <meta name="description" property="og:description" content="оксамитовий сезон у Bukovel">
    <meta name="twitter:description" content="оксамитовий сезон у Bukovel">
    <link rel="icon" type="image/png" href="img/favicon-16x16.png">
    <link rel="apple-touch-icon" type="image/png" href="img/favicon-56x56.png" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <!-- preloader begin -->
    <script>
        /* pace js 1.0.0 */
        (function(){var t,e,n,r,s,o,i,a,u,l,c,p,h,d,f,g,m,v,y,w,b,k,S,q,L,x,T,R,P,E,M,j,A,N,O,_,F,C,U,W,X,D,H,I,z,G,B,J,K,Q=[].slice,V={}.hasOwnProperty,Y=function(t,e){function n(){this.constructor=t}for(var r in e)V.call(e,r)&&(t[r]=e[r]);return n.prototype=e.prototype,t.prototype=new n,t.__super__=e.prototype,t},Z=[].indexOf||function(t){for(var e=0,n=this.length;n>e;e++)if(e in this&&this[e]===t)return e;return-1};for(b={catchupTime:100,initialRate:.03,minTime:250,ghostTime:100,maxProgressPerFrame:20,easeFactor:1.25,startOnPageLoad:!0,restartOnPushState:!0,restartOnRequestAfter:500,target:"body",elements:{checkInterval:100,selectors:["body"]},eventLag:{minSamples:10,sampleCount:3,lagThreshold:3},ajax:{trackMethods:["GET"],trackWebSockets:!0,ignoreURLs:[]}},P=function(){var t;return null!=(t="undefined"!=typeof performance&&null!==performance&&"function"==typeof performance.now?performance.now():void 0)?t:+new Date},M=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||window.msRequestAnimationFrame,w=window.cancelAnimationFrame||window.mozCancelAnimationFrame,null==M&&(M=function(t){return setTimeout(t,50)},w=function(t){return clearTimeout(t)}),A=function(t){var e,n;return e=P(),(n=function(){var r;return r=P()-e,r>=33?(e=P(),t(r,function(){return M(n)})):setTimeout(n,33-r)})()},j=function(){var t,e,n;return n=arguments[0],e=arguments[1],t=3<=arguments.length?Q.call(arguments,2):[],"function"==typeof n[e]?n[e].apply(n,t):n[e]},k=function(){var t,e,n,r,s,o,i;for(e=arguments[0],r=2<=arguments.length?Q.call(arguments,1):[],o=0,i=r.length;i>o;o++)if(n=r[o])for(t in n)V.call(n,t)&&(s=n[t],null!=e[t]&&"object"==typeof e[t]&&null!=s&&"object"==typeof s?k(e[t],s):e[t]=s);return e},m=function(t){var e,n,r,s,o;for(n=e=0,s=0,o=t.length;o>s;s++)r=t[s],n+=Math.abs(r),e++;return n/e},q=function(t,e){var n,r,s;if(null==t&&(t="options"),null==e&&(e=!0),s=document.querySelector("[data-pace-"+t+"]")){if(n=s.getAttribute("data-pace-"+t),!e)return n;try{return JSON.parse(n)}catch(o){return r=o,"undefined"!=typeof console&&null!==console?console.error("Error parsing inline pace options",r):void 0}}},i=function(){function t(){}return t.prototype.on=function(t,e,n,r){var s;return null==r&&(r=!1),null==this.bindings&&(this.bindings={}),null==(s=this.bindings)[t]&&(s[t]=[]),this.bindings[t].push({handler:e,ctx:n,once:r})},t.prototype.once=function(t,e,n){return this.on(t,e,n,!0)},t.prototype.off=function(t,e){var n,r,s;if(null!=(null!=(r=this.bindings)?r[t]:void 0)){if(null==e)return delete this.bindings[t];for(n=0,s=[];n<this.bindings[t].length;)s.push(this.bindings[t][n].handler===e?this.bindings[t].splice(n,1):n++);return s}},t.prototype.trigger=function(){var t,e,n,r,s,o,i,a,u;if(n=arguments[0],t=2<=arguments.length?Q.call(arguments,1):[],null!=(i=this.bindings)?i[n]:void 0){for(s=0,u=[];s<this.bindings[n].length;)a=this.bindings[n][s],r=a.handler,e=a.ctx,o=a.once,r.apply(null!=e?e:this,t),u.push(o?this.bindings[n].splice(s,1):s++);return u}},t}(),l=window.Pace||{},window.Pace=l,k(l,i.prototype),E=l.options=k({},b,window.paceOptions,q()),B=["ajax","document","eventLag","elements"],H=0,z=B.length;z>H;H++)F=B[H],E[F]===!0&&(E[F]=b[F]);u=function(t){function e(){return J=e.__super__.constructor.apply(this,arguments)}return Y(e,t),e}(Error),e=function(){function t(){this.progress=0}return t.prototype.getElement=function(){var t;if(null==this.el){if(t=document.querySelector(E.target),!t)throw new u;this.el=document.createElement("div"),this.el.className="pace pace-active",document.body.className=document.body.className.replace(/pace-done/g,""),document.body.className+=" pace-running",this.el.innerHTML='<div class="pace-progress">\n  <div id="bookloading-wrapper"><div id="bookloading-cloud"></div><div id="bookloading-snowflakes"><div class="snowflake-1"></div><div class="snowflake-2"></div><div class="snowflake-3"></div><div class="snowflake-4"></div><div class="snowflake-5"></div><div class="snowflake-6"></div><div class="snowflake-7"></div><div class="snowflake-8"></div><div class="snowflake-9"></div><div class="snowflake-10"></div></div></div><div class="pace-progress-inner"></div>\n</div>\n<div class="pace-activity"></div>',null!=t.firstChild?t.insertBefore(this.el,t.firstChild):t.appendChild(this.el)}return this.el},t.prototype.finish=function(){var t;return t=this.getElement(),t.className=t.className.replace("pace-active",""),t.className+=" pace-inactive",document.body.className=document.body.className.replace("pace-running",""),document.body.className+=" pace-done"},t.prototype.update=function(t){return this.progress=t,this.render()},t.prototype.destroy=function(){try{this.getElement().parentNode.removeChild(this.getElement())}catch(t){u=t}return this.el=void 0},t.prototype.render=function(){var t,e,n,r,s,o,i;if(null==document.querySelector(E.target))return!1;for(t=this.getElement(),r="translate3d("+this.progress+"%, 0, 0)",i=["webkitTransform","msTransform","transform"],s=0,o=i.length;o>s;s++)e=i[s],t.children[0].style[e]=r;return(!this.lastRenderedProgress||this.lastRenderedProgress|0!==this.progress|0)&&(t.children[0].setAttribute("data-progress-text",""+(0|this.progress)+"%"),this.progress>=100?n="99":(n=this.progress<10?"0":"",n+=0|this.progress),t.children[0].setAttribute("data-progress",""+n)),this.lastRenderedProgress=this.progress},t.prototype.done=function(){return this.progress>=100},t}(),a=function(){function t(){this.bindings={}}return t.prototype.trigger=function(t,e){var n,r,s,o,i;if(null!=this.bindings[t]){for(o=this.bindings[t],i=[],r=0,s=o.length;s>r;r++)n=o[r],i.push(n.call(this,e));return i}},t.prototype.on=function(t,e){var n;return null==(n=this.bindings)[t]&&(n[t]=[]),this.bindings[t].push(e)},t}(),D=window.XMLHttpRequest,X=window.XDomainRequest,W=window.WebSocket,S=function(t,e){var n,r,s,o;o=[];for(r in e.prototype)try{s=e.prototype[r],o.push(null==t[r]&&"function"!=typeof s?t[r]=s:void 0)}catch(i){n=i}return o},T=[],l.ignore=function(){var t,e,n;return e=arguments[0],t=2<=arguments.length?Q.call(arguments,1):[],T.unshift("ignore"),n=e.apply(null,t),T.shift(),n},l.track=function(){var t,e,n;return e=arguments[0],t=2<=arguments.length?Q.call(arguments,1):[],T.unshift("track"),n=e.apply(null,t),T.shift(),n},_=function(t){var e;if(null==t&&(t="GET"),"track"===T[0])return"force";if(!T.length&&E.ajax){if("socket"===t&&E.ajax.trackWebSockets)return!0;if(e=t.toUpperCase(),Z.call(E.ajax.trackMethods,e)>=0)return!0}return!1},c=function(t){function e(){var t,n=this;e.__super__.constructor.apply(this,arguments),t=function(t){var e;return e=t.open,t.open=function(r,s){return _(r)&&n.trigger("request",{type:r,url:s,request:t}),e.apply(t,arguments)}},window.XMLHttpRequest=function(e){var n;return n=new D(e),t(n),n};try{S(window.XMLHttpRequest,D)}catch(r){}if(null!=X){window.XDomainRequest=function(){var e;return e=new X,t(e),e};try{S(window.XDomainRequest,X)}catch(r){}}if(null!=W&&E.ajax.trackWebSockets){window.WebSocket=function(t,e){var r;return r=null!=e?new W(t,e):new W(t),_("socket")&&n.trigger("request",{type:"socket",url:t,protocols:e,request:r}),r};try{S(window.WebSocket,W)}catch(r){}}}return Y(e,t),e}(a),I=null,L=function(){return null==I&&(I=new c),I},O=function(t){var e,n,r,s;for(s=E.ajax.ignoreURLs,n=0,r=s.length;r>n;n++)if(e=s[n],"string"==typeof e){if(-1!==t.indexOf(e))return!0}else if(e.test(t))return!0;return!1},L().on("request",function(e){var n,r,s,o,i;return o=e.type,s=e.request,i=e.url,O(i)?void 0:l.running||E.restartOnRequestAfter===!1&&"force"!==_(o)?void 0:(r=arguments,n=E.restartOnRequestAfter||0,"boolean"==typeof n&&(n=0),setTimeout(function(){var e,n,i,a,u,c;if(e="socket"===o?s.readyState<2:0<(a=s.readyState)&&4>a){for(l.restart(),u=l.sources,c=[],n=0,i=u.length;i>n;n++){if(F=u[n],F instanceof t){F.watch.apply(F,r);break}c.push(void 0)}return c}},n))}),t=function(){function t(){var t=this;this.elements=[],L().on("request",function(){return t.watch.apply(t,arguments)})}return t.prototype.watch=function(t){var e,n,r,s;return r=t.type,e=t.request,s=t.url,O(s)?void 0:(n="socket"===r?new d(e):new f(e),this.elements.push(n))},t}(),f=function(){function t(t){var e,n,r,s,o,i,a=this;if(this.progress=0,null!=window.ProgressEvent)for(n=null,t.addEventListener("progress",function(t){return a.progress=t.lengthComputable?100*t.loaded/t.total:a.progress+(100-a.progress)/2},!1),i=["load","abort","timeout","error"],r=0,s=i.length;s>r;r++)e=i[r],t.addEventListener(e,function(){return a.progress=100},!1);else o=t.onreadystatechange,t.onreadystatechange=function(){var e;return 0===(e=t.readyState)||4===e?a.progress=100:3===t.readyState&&(a.progress=50),"function"==typeof o?o.apply(null,arguments):void 0}}return t}(),d=function(){function t(t){var e,n,r,s,o=this;for(this.progress=0,s=["error","open"],n=0,r=s.length;r>n;n++)e=s[n],t.addEventListener(e,function(){return o.progress=100},!1)}return t}(),r=function(){function t(t){var e,n,r,o;for(null==t&&(t={}),this.elements=[],null==t.selectors&&(t.selectors=[]),o=t.selectors,n=0,r=o.length;r>n;n++)e=o[n],this.elements.push(new s(e))}return t}(),s=function(){function t(t){this.selector=t,this.progress=0,this.check()}return t.prototype.check=function(){var t=this;return document.querySelector(this.selector)?this.done():setTimeout(function(){return t.check()},E.elements.checkInterval)},t.prototype.done=function(){return this.progress=100},t}(),n=function(){function t(){var t,e,n=this;this.progress=null!=(e=this.states[document.readyState])?e:100,t=document.onreadystatechange,document.onreadystatechange=function(){return null!=n.states[document.readyState]&&(n.progress=n.states[document.readyState]),"function"==typeof t?t.apply(null,arguments):void 0}}return t.prototype.states={loading:0,interactive:50,complete:100},t}(),o=function(){function t(){var t,e,n,r,s,o=this;this.progress=0,t=0,s=[],r=0,n=P(),e=setInterval(function(){var i;return i=P()-n-50,n=P(),s.push(i),s.length>E.eventLag.sampleCount&&s.shift(),t=m(s),++r>=E.eventLag.minSamples&&t<E.eventLag.lagThreshold?(o.progress=100,clearInterval(e)):o.progress=100*(3/(t+3))},50)}return t}(),h=function(){function t(t){this.source=t,this.last=this.sinceLastUpdate=0,this.rate=E.initialRate,this.catchup=0,this.progress=this.lastProgress=0,null!=this.source&&(this.progress=j(this.source,"progress"))}return t.prototype.tick=function(t,e){var n;return null==e&&(e=j(this.source,"progress")),e>=100&&(this.done=!0),e===this.last?this.sinceLastUpdate+=t:(this.sinceLastUpdate&&(this.rate=(e-this.last)/this.sinceLastUpdate),this.catchup=(e-this.progress)/E.catchupTime,this.sinceLastUpdate=0,this.last=e),e>this.progress&&(this.progress+=this.catchup*t),n=1-Math.pow(this.progress/100,E.easeFactor),this.progress+=n*this.rate*t,this.progress=Math.min(this.lastProgress+E.maxProgressPerFrame,this.progress),this.progress=Math.max(0,this.progress),this.progress=Math.min(100,this.progress),this.lastProgress=this.progress,this.progress},t}(),C=null,N=null,v=null,U=null,g=null,y=null,l.running=!1,x=function(){return E.restartOnPushState?l.restart():void 0},null!=window.history.pushState&&(G=window.history.pushState,window.history.pushState=function(){return x(),G.apply(window.history,arguments)}),null!=window.history.replaceState&&(K=window.history.replaceState,window.history.replaceState=function(){return x(),K.apply(window.history,arguments)}),p={ajax:t,elements:r,document:n,eventLag:o},(R=function(){var t,n,r,s,o,i,a,u;for(l.sources=C=[],i=["ajax","elements","document","eventLag"],n=0,s=i.length;s>n;n++)t=i[n],E[t]!==!1&&C.push(new p[t](E[t]));for(u=null!=(a=E.extraSources)?a:[],r=0,o=u.length;o>r;r++)F=u[r],C.push(new F(E));return l.bar=v=new e,N=[],U=new h})(),l.stop=function(){return l.trigger("stop"),l.running=!1,v.destroy(),y=!0,null!=g&&("function"==typeof w&&w(g),g=null),R()},l.restart=function(){return l.trigger("restart"),l.stop(),l.start()},l.go=function(){var t;return l.running=!0,v.render(),t=P(),y=!1,g=A(function(e,n){var r,s,o,i,a,u,c,p,d,f,g,m,w,b,k,S;for(p=100-v.progress,s=g=0,o=!0,u=m=0,b=C.length;b>m;u=++m)for(F=C[u],f=null!=N[u]?N[u]:N[u]=[],a=null!=(S=F.elements)?S:[F],c=w=0,k=a.length;k>w;c=++w)i=a[c],d=null!=f[c]?f[c]:f[c]=new h(i),o&=d.done,d.done||(s++,g+=d.tick(e));return r=g/s,v.update(U.tick(e,r)),v.done()||o||y?(v.update(100),l.trigger("done"),setTimeout(function(){return v.finish(),l.running=!1,l.trigger("hide")},Math.max(E.ghostTime,Math.max(E.minTime-(P()-t),0)))):n()})},l.start=function(t){k(E,t),l.running=!0;try{v.render()}catch(e){u=e}return document.querySelector(".pace")?(l.trigger("start"),l.go()):setTimeout(l.start,50)},"function"==typeof define&&define.amd?define(function(){return l}):"object"==typeof exports?module.exports=l:E.startOnPageLoad&&l.start()}).call(this);/* pace js end */
    </script>
    <style>.pace{-webkit-pointer-events:none;pointer-events:none;-webkit-user-select:none;-moz-user-select:none;user-select:none}.pace.pace-inactive .pace-progress{display:none}.pace .pace-progress{position:fixed;z-index:2000;top:0;right:0;height:100%;width:100%;-moz-transform:translate3d(0,0,0)!important;-webkit-transform:translate3d(0,0,0)!important;-ms-transform:translate3d(0,0,0)!important;transform:translate3d(0,0,0)!important;text-align:center;background:-moz-radial-gradient(center,ellipse cover,rgba(0,118,188) 0,rgba(0,118,188,1) 50%,rgba(0,118,188,1) 50%,rgba(82,172,215,1) 100%);background:-webkit-gradient(radial,center center,0,center center,100%,color-stop(0,rgba(0,118,188)),color-stop(50%,rgba(168,213,235,1)),color-stop(50%,rgba(168,213,235,1)),color-stop(100%,rgba(82,172,215,1)));background:-webkit-radial-gradient(center,ellipse cover,rgba(0,118,188) 0,rgba(0,118,188,1) 50%,rgba(0,118,188,1) 50%,rgba(82,172,215,1) 100%);background:-o-radial-gradient(center,ellipse cover,rgba(0,118,188,1) 0,rgba(0,118,188,1) 50%,rgba(0,118,188,1) 50%,rgba(82,172,215,1) 100%);background:-ms-radial-gradient(center,ellipse cover,rgba(0,118,188,1) 0,rgba(0,118,188,1) 50%,rgba(0,118,188,1) 50%,rgba(82,172,215,1) 100%);background:radial-gradient(ellipse at center,rgba(0,118,188,1) 0,rgba(0,118,188,1) 50%,rgba(0,118,188,1) 50%,rgba(82,172,215,1) 100%);filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#1697dd', endColorstr='#ffffff', GradientType=1 )}#bookloading-cloud,#bookloading-snowflakes div{background-position:center;background-repeat:no-repeat}.pace .pace-progress:after{display:block;position:relative;top:44%;right:.5rem;content:attr(data-progress-text);font-family:Roboto,sans-serif;font-weight:900;font-size:50px;padding-top:70px;padding-left:30px;line-height:1;text-align:center;color:#fff}#bookloading-wrapper{margin-top:-193.5px;height:387px;position:absolute;top:50%;left:0;right:0}#bookloading-cloud{height:106px;background-image:url(img/cloud.png);display:block}#bookloading-snowflakes{height:132px;display:block;position:relative}#bookloading-snowflakes div{position:absolute;height:24px;width:23px;background-image:url(img/snow.png);margin-top:10px;margin-bottom:20px}#bookloading-snowflakes .snowflake-1,#bookloading-snowflakes .snowflake-2,#bookloading-snowflakes .snowflake-3,#bookloading-snowflakes .snowflake-4{left:50%;top:0}#bookloading-snowflakes .snowflake-5,#bookloading-snowflakes .snowflake-6,#bookloading-snowflakes .snowflake-7{left:50%;top:34px}@media screen and (max-width:600px){#bookloading-cloud{height:53px;background-size:100px;margin-top:50px}#bookloading-snowflakes{height:66px}#bookloading-snowflakes div{height:12px;width:12px;background-size:100%}#bookloading-snowflakes .snowflake-5,#bookloading-snowflakes .snowflake-6,#bookloading-snowflakes .snowflake-7{top:17px}}#bookloading-snowflakes .snowflake-10,#bookloading-snowflakes .snowflake-8,#bookloading-snowflakes .snowflake-9{left:50%;top:68px}#bookloading-snowflakes .snowflake-1{-webkit-animation:flash 6.5s -.6s ease both;-moz-animation:flash 6.5s -.6s ease both;-ms-animation:flash 6.5s -.6s ease both;-o-animation:flash 6.5s -.6s ease both;animation:flash 6.5s -.6s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:73px}#bookloading-snowflakes .snowflake-2{-webkit-animation:flash 6.4s -1.2s ease both;-moz-animation:flash 6.4s -1.2s ease both;-ms-animation:flash 6.4s -1.2s ease both;-o-animation:flash 6.4s -1.2s ease both;animation:flash 6.4s -1.2s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:23px}#bookloading-snowflakes .snowflake-3{-webkit-animation:flash 6.6s -3s ease both;-moz-animation:flash 6.6s -3s ease both;-ms-animation:flash 6.6s -3s ease both;-o-animation:flash 6.6s -3s ease both;animation:flash 6.6s -3s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:-27px}#bookloading-snowflakes .snowflake-4{-webkit-animation:flash 6.7s -4.5s ease both;-moz-animation:flash 6.7s -4.5s ease both;-ms-animation:flash 6.7s -4.5s ease both;-o-animation:flash 6.7s -4.5s ease both;animation:flash 6.7s -4.5s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:-77px}#bookloading-snowflakes .snowflake-5{-webkit-animation:flash 6.6s -2.25s ease both;-moz-animation:flash 6.6s -2.25s ease both;-ms-animation:flash 6.6s -2.25s ease both;-o-animation:flash 6.6s -2.25s ease both;animation:flash 6.6s -2.25s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:49px}#bookloading-snowflakes .snowflake-6{-webkit-animation:flash 6.6s -1.45s ease both;-moz-animation:flash 6.6s -1.45s ease both;-ms-animation:flash 6.6s -1.45s ease both;-o-animation:flash 6.6s -1.45s ease both;animation:flash 6.6s -1.45s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:-4px}#bookloading-snowflakes .snowflake-7{-webkit-animation:flash 6.8s -2.06s ease both;-moz-animation:flash 6.8s -2.06s ease both;-ms-animation:flash 6.8s -2.06s ease both;-o-animation:flash 6.8s -2.06s ease both;animation:flash 6.8s -2.06s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:-57px}#bookloading-snowflakes .snowflake-8{-webkit-animation:flash 6.2s -1.25s ease both;-moz-animation:flash 6.2s -1.25s ease both;-ms-animation:flash 6.2s -1.25s ease both;-o-animation:flash 6.2s -1.25s ease both;animation:flash 6.2s -1.25s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:69px}#bookloading-snowflakes .snowflake-9{-webkit-animation:flash 6.3s -2.4s ease both;-moz-animation:flash 6.3s -2.4s ease both;-ms-animation:flash 6.3s -2.4s ease both;-o-animation:flash 6.3s -2.4s ease both;animation:flash 6.3s -2.4s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:16px}#bookloading-snowflakes .snowflake-10{-webkit-animation:flash 6.7s -3.2s ease both;-moz-animation:flash 6.7s -3.2s ease both;-ms-animation:flash 6.7s -3.2s ease both;-o-animation:flash 6.7s -3.2s ease both;animation:flash 6.7s -3.2s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:-37px}@media screen and (max-width:600px){#bookloading-snowflakes .snowflake-10,#bookloading-snowflakes .snowflake-8,#bookloading-snowflakes .snowflake-9{top:34px}#bookloading-snowflakes .snowflake-1{-webkit-animation:flash 6.5s -.6s ease both;-moz-animation:flash 6.5s -.6s ease both;-ms-animation:flash 6.5s -.6s ease both;-o-animation:flash 6.5s -.6s ease both;animation:flash 6.5s -.6s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:30px}#bookloading-snowflakes .snowflake-2{-webkit-animation:flash 6.4s -1.2s ease both;-moz-animation:flash 6.4s -1.2s ease both;-ms-animation:flash 6.4s -1.2s ease both;-o-animation:flash 6.4s -1.2s ease both;animation:flash 6.4s -1.2s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:10px}#bookloading-snowflakes .snowflake-3{-webkit-animation:flash 6.6s -3s ease both;-moz-animation:flash 6.6s -3s ease both;-ms-animation:flash 6.6s -3s ease both;-o-animation:flash 6.6s -3s ease both;animation:flash 6.6s -3s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:-10px}#bookloading-snowflakes .snowflake-4{-webkit-animation:flash 6.7s -4.5s ease both;-moz-animation:flash 6.7s -4.5s ease both;-ms-animation:flash 6.7s -4.5s ease both;-o-animation:flash 6.7s -4.5s ease both;animation:flash 6.7s -4.5s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:-30px}#bookloading-snowflakes .snowflake-5{-webkit-animation:flash 6.6s -2.25s ease both;-moz-animation:flash 6.6s -2.25s ease both;-ms-animation:flash 6.6s -2.25s ease both;-o-animation:flash 6.6s -2.25s ease both;animation:flash 6.6s -2.25s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:20px}#bookloading-snowflakes .snowflake-6{-webkit-animation:flash 6.6s -1.45s ease both;-moz-animation:flash 6.6s -1.45s ease both;-ms-animation:flash 6.6s -1.45s ease both;-o-animation:flash 6.6s -1.45s ease both;animation:flash 6.6s -1.45s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:0}#bookloading-snowflakes .snowflake-7{-webkit-animation:flash 6.8s -2.06s ease both;-moz-animation:flash 6.8s -2.06s ease both;-ms-animation:flash 6.8s -2.06s ease both;-o-animation:flash 6.8s -2.06s ease both;animation:flash 6.8s -2.06s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:-20px}#bookloading-snowflakes .snowflake-8{-webkit-animation:flash 6.2s -1.25s ease both;-moz-animation:flash 6.2s -1.25s ease both;-ms-animation:flash 6.2s -1.25s ease both;-o-animation:flash 6.2s -1.25s ease both;animation:flash 6.2s -1.25s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:25px}#bookloading-snowflakes .snowflake-9{-webkit-animation:flash 6.3s -2.4s ease both;-moz-animation:flash 6.3s -2.4s ease both;-ms-animation:flash 6.3s -2.4s ease both;-o-animation:flash 6.3s -2.4s ease both;animation:flash 6.3s -2.4s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:5px}#bookloading-snowflakes .snowflake-10{-webkit-animation:flash 6.7s -3.2s ease both;-moz-animation:flash 6.7s -3.2s ease both;-ms-animation:flash 6.7s -3.2s ease both;-o-animation:flash 6.7s -3.2s ease both;animation:flash 6.7s -3.2s ease both;-webkit-animation-iteration-count:infinite;-moz-animation-iteration-count:infinite;-ms-animation-iteration-count:infinite;-o-animation-iteration-count:infinite;animation-iteration-count:infinite;margin-left:-15px}}@-webkit-keyframes flash{0%,100%,50%{opacity:1}25%,75%{opacity:0}}</style>
    <!-- preloader end -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript>
    <iframe src="//www.googletagmanager.com/ns.html?id=GTM-KVG88L"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KVG88L');
</script>
<!-- End Google Tag Manager -->
<div class="section section-section1" id="section1">
    <div class="bg1"></div>
    <div class="overlay">
    <div class="slogan" id="particle-slider">
        <div class="slides">
            <div id="first-slide" class="slide" data-src='data:image/svg+xml;utf8,<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="999.909" height="250.215" viewBox="472 -120.3 1000.2 296.3" style="enable-background:new 472 -120.3 1000.2 296.3;">
<style type="text/css">
	.st0{fill:#fff;}
</style>
<path class="st0" d="M1137.1,54v9.9l8.8,5.5v-9.7l-7.8-5l2-3.5l5.8,3.5v-6.8h4v6.8l6.1-4l2.2,3.6l-8.3,5.3v9.8l8.2-5.2l-0.1-9.4h4.1
	l0.1,6.7l5.6-3.9l2.2,3.4l-5.7,3.8l6.6,3.1l-1.6,3.8l-9-4.3l-8.7,5.7l8.7,5.3l8.5-4.1l1.6,3.7l-6.1,3.2l5.8,3.8l-2.1,3.3l-5.8-3.6
	V92h-4V82l-8.4-5.2v10.3l8,5l-2.2,3.4l-5.8-3.7v6.5h-4v-6.4l-6.3,4l-2-3.5l8.3-5.4V76.8l-8.5,5.6l0.1,9.4h-3.9l-0.1-6.8l-5.5,3.5
	l-2.2-3.4l5.4-3.4l-6.6-3.1l1.6-4l9,4.4l9-5.9l-9-5.7l-8.6,4l-1.4-3.7l6.1-2.9l-5.5-3.4l2-3.5l5.6,3.5V54H1137.1z M1117.1,111.4
	c-6.5-2.3-12.3-4.3-17.8-6.1c-22.2-7.7-37.8-12.3-46.6-13.7c-17.1-2.8-36.9-0.5-59.7,7l20.2,19.3l-43.7-22.1
	c-11.7-4.6-22-6.5-30.7-5.3c-8.4,1.2-20,5.9-35,13.7l9.8,13.7l-20.8-16.1c-11.5-8.4-21.1-11.3-28.7-8.7
	c-5.7,1.7-17.9,10.2-36.8,25.9c16.6-27.6,29-42.8,37.4-45.6c8.7-3.2,20,4.3,33.7,22.7C915.1,79.5,929,70.6,940,69.4
	c11.2-1.2,25,5.2,41,18.8c25.6-15.9,48.6-21.7,68.8-17c11.6,2.8,28,10.5,49.5,23.6c5.5,3.5,11.6,7.2,17.8,11.3
	c5.4,3.5,11.2,7.5,17.2,11.7C1128.3,115.5,1122.4,113.5,1117.1,111.4z M852.6,164.6v-54.9h10.6v18.1c0,1.3,0,2.5,0,3.6
	c0,1.1,0,1.9-0.1,2.5h0.1c0.7-0.5,1.4-0.9,2.2-1.4c0.8-0.5,1.7-0.9,2.6-1.2s2-0.6,3.1-0.9c1.1-0.2,2.3-0.3,3.5-0.3
	c2.4,0,4.8,0.4,7,1.3c2.3,0.9,4.3,2.2,6.1,4c1.8,1.8,3.2,4.1,4.2,7c1,2.8,1.6,6.1,1.6,9.8c0,3.9-0.6,7.4-1.7,10.4
	c-1.1,3-2.6,5.5-4.4,7.5c-1.8,1.9-3.9,3.4-6.2,4.3c-2.3,0.9-4.6,1.4-6.9,1.4c-1.6,0-3.1-0.2-4.4-0.6c-1.3-0.4-2.5-0.9-3.4-1.5
	c-1-0.6-1.8-1.2-2.5-1.9c-0.7-0.7-1.2-1.2-1.6-1.8h-0.2l-1,4.6h-8.7c0.1-1.3,0.1-2.9,0.2-4.8C852.5,168.1,852.6,166.3,852.6,164.6z
	 M863.1,163c0.4,0.5,0.9,1,1.5,1.5c0.6,0.5,1.3,1,2.1,1.4c0.8,0.4,1.6,0.8,2.5,1.1c0.9,0.3,1.8,0.4,2.9,0.4c1.7,0,3.2-0.4,4.5-1.1
	c1.3-0.7,2.4-1.7,3.3-3c0.9-1.3,1.5-2.8,2-4.6c0.4-1.8,0.7-3.8,0.7-6c0-2.7-0.3-5.1-1-7c-0.7-2-1.5-3.5-2.6-4.5
	c-1.1-1-2.2-1.7-3.3-2.1c-1.1-0.4-2.3-0.5-3.4-0.5c-1.5,0-3.1,0.3-4.7,0.9c-1.6,0.6-3.1,1.5-4.4,2.5V163z M913,156.7
	c0,1.5,0.1,2.7,0.3,3.8c0.2,1.1,0.5,2,0.9,2.8c0.7,1.3,1.7,2.3,3,2.9c1.3,0.6,2.9,1,4.7,1c1.9,0,3.5-0.4,4.9-1.1
	c1.4-0.7,2.4-1.8,3.1-3.3c0.7-1.4,1-3.4,1-5.9v-25.7h10.6v26.6c0,3.5-0.6,6.5-1.8,9c-0.7,1.4-1.6,2.6-2.7,3.8
	c-1.1,1.1-2.4,2.1-3.8,2.9c-1.5,0.8-3.1,1.4-5,1.8c-1.9,0.4-3.9,0.7-6.2,0.7c-4.3,0-7.9-0.7-10.8-2.2c-2.9-1.5-5-3.5-6.5-6
	c-0.8-1.3-1.3-2.8-1.7-4.5c-0.4-1.6-0.6-3.4-0.6-5.4v-26.6H913V156.7z M963.6,174.8H953v-65.1h10.6V174.8z M991.8,131.2L977.7,152
	l17.7,22.8h-12.5l-17.3-22.7l13.7-20.8H991.8z M1003.8,136.5c1.9-2,4.2-3.6,6.9-4.7c2.7-1.1,5.7-1.6,9-1.6c3.4,0,6.4,0.5,9.1,1.6
	c2.7,1.1,5,2.6,6.9,4.7c1.9,2,3.3,4.4,4.3,7.2c1,2.8,1.5,5.9,1.5,9.4c0,3.4-0.5,6.5-1.5,9.4c-1,2.8-2.5,5.2-4.4,7.2
	c-1.9,2-4.2,3.6-6.9,4.7c-2.7,1.1-5.7,1.6-9,1.6c-3.4,0-6.4-0.5-9.1-1.6c-2.7-1.1-5-2.6-6.9-4.7c-1.9-2-3.3-4.4-4.3-7.2
	c-1-2.8-1.5-5.9-1.5-9.4c0-3.4,0.5-6.5,1.5-9.4C1000.5,140.9,1001.9,138.5,1003.8,136.5z M1009.7,159.2c0.5,1.8,1.3,3.2,2.3,4.4
	c1,1.2,2.1,2.1,3.4,2.7c1.3,0.6,2.8,0.9,4.4,0.9c1.6,0,3-0.3,4.3-0.9c1.3-0.6,2.4-1.5,3.4-2.7c1-1.2,1.7-2.7,2.2-4.4
	c0.5-1.8,0.8-3.8,0.8-6.1c0-2.3-0.3-4.3-0.8-6.1c-0.5-1.8-1.3-3.3-2.2-4.5s-2.1-2.1-3.4-2.7c-1.3-0.6-2.8-0.9-4.4-0.9
	c-1.5,0-3,0.3-4.3,0.9c-1.3,0.6-2.5,1.5-3.4,2.7c-1,1.2-1.7,2.7-2.2,4.5c-0.5,1.8-0.8,3.8-0.8,6.1
	C1008.9,155.4,1009.2,157.4,1009.7,159.2z M1062.6,147.9c1.7,4.7,3.3,9.4,4.7,14h0.2c1.3-4,2.8-8.6,4.6-13.8l6-16.9h11.2l-16.7,43.6
	h-10.5l-16.8-43.6h11.2L1062.6,147.9z M1099.7,136.1c1.9-1.9,4.1-3.4,6.5-4.4c2.5-1,5-1.5,7.6-1.5c2.7,0,5.3,0.4,7.6,1.3
	c2.3,0.8,4.3,2.2,6,4c1.7,1.8,3,4.1,3.9,6.9c0.9,2.8,1.4,6.2,1.4,10.1c0,1.5-0.1,2.8-0.1,3.8h-28.3c0.1,2,0.6,3.7,1.3,5.2
	c0.7,1.4,1.7,2.6,2.9,3.5c1.2,0.9,2.6,1.5,4.2,1.9c1.6,0.4,3.3,0.6,5.2,0.6c2.1,0,4.2-0.2,6.4-0.6c2.2-0.4,4.5-1,6.8-1.8v8.5
	c-1.4,0.6-3.5,1.2-6.3,1.7c-2.8,0.5-5.8,0.8-8.8,0.8c-3.1,0-6-0.4-8.7-1.2c-2.8-0.8-5.2-2.1-7.2-3.8c-2.1-1.8-3.7-4.1-4.9-6.9
	c-1.2-2.8-1.8-6.2-1.8-10.3c0-4,0.6-7.4,1.7-10.4C1096.3,140.5,1097.8,138,1099.7,136.1z M1121.8,147.9c0-1.3-0.2-2.5-0.5-3.6
	c-0.3-1.1-0.8-2.1-1.5-3c-0.7-0.9-1.5-1.5-2.5-2c-1-0.5-2.2-0.7-3.5-0.7c-1.3,0-2.5,0.2-3.6,0.7c-1,0.5-1.9,1.2-2.7,2
	c-0.8,0.9-1.4,1.9-1.8,3c-0.4,1.1-0.7,2.4-0.9,3.6H1121.8z M1153.6,174.9H1143v-65.1h10.6V174.9z"/>
<g>
	<path class="st0" d="M531-29.4c0,8.1-2.5,14.7-7.6,20c-5.1,5.2-11.7,7.9-19.8,7.9c-8.1,0-14.7-2.6-19.7-7.9
		c-5-5.2-7.6-11.9-7.6-19.9v-11.8c0-8,2.5-14.6,7.5-19.9c5-5.3,11.6-7.9,19.6-7.9c8.1,0,14.7,2.6,19.8,7.9
		c5.1,5.3,7.6,11.9,7.6,19.9V-29.4z M517.9-41.3c0-5.1-1.3-9.3-3.8-12.6c-2.6-3.3-6.1-4.9-10.6-4.9c-4.4,0-7.9,1.6-10.4,4.9
		c-2.5,3.3-3.7,7.5-3.7,12.6v11.9c0,5.2,1.2,9.4,3.7,12.7c2.5,3.3,6,4.9,10.4,4.9c4.5,0,8-1.6,10.5-4.9c2.5-3.3,3.8-7.5,3.8-12.7
		V-41.3z"/>
	<path class="st0" d="M559.4-29.9h-4.8v27.4h-13.1v-65.5h13.1v26.8h3.7l18-26.8h16.2l-22.7,30.5l24.4,35.1h-16.7L559.4-29.9z"/>
	<path class="st0" d="M647.9-24.2L648-24c0.1,7-2,12.5-6.4,16.4c-4.4,4-10.4,5.9-18.1,5.9c-7.9,0-14.2-2.5-19-7.6
		c-4.8-5.1-7.2-11.6-7.2-19.6v-13c0-8,2.4-14.5,7.1-19.6s10.9-7.6,18.5-7.6c8,0,14.2,2,18.7,5.8c4.5,3.9,6.7,9.4,6.6,16.5l-0.1,0.3
		h-12.8c0-4.2-1-7.3-3-9.4c-2-2.1-5.2-3.1-9.4-3.1c-3.9,0-7,1.6-9.2,4.7c-2.2,3.2-3.3,7.3-3.3,12.3v13.1c0,5,1.2,9.1,3.5,12.3
		c2.3,3.2,5.6,4.8,9.7,4.8c3.9,0,6.9-1,8.8-3.1c1.9-2.1,2.9-5.2,2.9-9.5H647.9z"/>
	<path class="st0" d="M691.5-16.7h-21.2l-4.4,14.1h-13.7l22.1-65.5h13.5l22,65.5H696L691.5-16.7z M673.5-27h14.8l-7.2-23.1h-0.3
		L673.5-27z"/>
	<path class="st0" d="M749.9-19.8h0.3L767-68.1h17.1v65.5H771v-44.7l-0.3,0L754.5-2.6h-8.9l-16.2-44.9l-0.3,0v44.8H716v-65.5h17.2
		L749.9-19.8z"/>
	<path class="st0" d="M835.6-68.1h13.1v65.5h-13.1v-43.8l-0.3,0L808.8-2.6h-13.1v-65.5h13.1v43.7l0.3,0L835.6-68.1z"/>
	<path class="st0" d="M904.9-57.9h-18.7v55.4h-13.1v-55.4h-18.4v-10.1h50.3V-57.9z"/>
	<path class="st0" d="M963.9-29.4c0,8.1-2.5,14.7-7.6,20c-5.1,5.2-11.7,7.9-19.8,7.9c-8.1,0-14.7-2.6-19.7-7.9
		c-5-5.2-7.6-11.9-7.6-19.9v-11.8c0-8,2.5-14.6,7.5-19.9c5-5.3,11.6-7.9,19.6-7.9c8.1,0,14.7,2.6,19.8,7.9
		c5.1,5.3,7.6,11.9,7.6,19.9V-29.4z M950.8-41.3c0-5.1-1.3-9.3-3.8-12.6c-2.6-3.3-6.1-4.9-10.6-4.9c-4.4,0-7.9,1.6-10.4,4.9
		c-2.5,3.3-3.7,7.5-3.7,12.6v11.9c0,5.2,1.2,9.4,3.7,12.7c2.5,3.3,6,4.9,10.4,4.9c4.5,0,8-1.6,10.5-4.9c2.5-3.3,3.8-7.5,3.8-12.7
		V-41.3z"/>
	<path class="st0" d="M974.3-2.6v-65.5h21.5c7.7,0,13.7,1.5,18,4.5s6.5,7.5,6.5,13.4c0,3-0.8,5.7-2.3,8.1c-1.6,2.4-3.8,4.2-6.8,5.4
		c4.1,0.8,7.2,2.5,9.2,5.3c2.1,2.8,3.1,6,3.1,9.8c0,6.3-2.1,11-6.2,14.2c-4.1,3.2-10,4.8-17.6,4.8H974.3z M987.4-40.6h8.8
		c3.5,0,6.2-0.7,8.1-2.2c1.9-1.4,2.8-3.5,2.8-6.3c0-3-1-5.3-2.9-6.7c-1.9-1.4-4.7-2.2-8.5-2.2h-8.3V-40.6z M987.4-31.5v18.9h12.2
		c3.5,0,6.1-0.8,8-2.3c1.8-1.5,2.7-3.8,2.7-6.7c0-3.2-0.9-5.6-2.7-7.3c-1.8-1.7-4.5-2.6-8.1-2.6H987.4z"/>
	<path class="st0" d="M1072.4-68.1h13.1v65.5h-13.1v-43.8l-0.3,0l-26.5,43.8h-13.1v-65.5h13.1v43.7l0.3,0L1072.4-68.1z"/>
	<path class="st0" d="M1136.8-68.1h13.1v65.5h-13.1v-43.8l-0.3,0L1110-2.6h-13.1v-65.5h13.1v43.7l0.3,0L1136.8-68.1z M1138.7-87.9
		l0.1,0.3c0.2,3.8-1.2,6.9-4,9.3c-2.8,2.4-6.6,3.6-11.3,3.6c-4.8,0-8.6-1.2-11.4-3.6c-2.8-2.4-4.1-5.5-4-9.3l0.1-0.3h9.6
		c0,1.6,0.5,2.9,1.4,3.9c0.9,1,2.3,1.5,4.3,1.5c1.9,0,3.3-0.5,4.2-1.5c0.9-1,1.4-2.3,1.4-3.9H1138.7z"/>
	<path class="st0" d="M1233.9-24.2l0.1,0.3c0.1,7-2,12.5-6.4,16.4c-4.4,4-10.4,5.9-18.1,5.9c-7.9,0-14.2-2.5-19-7.6
		c-4.8-5.1-7.2-11.6-7.2-19.6v-13c0-8,2.4-14.5,7.1-19.6c4.7-5.1,10.9-7.6,18.5-7.6c8,0,14.2,2,18.7,5.8c4.5,3.9,6.7,9.4,6.6,16.5
		l-0.1,0.3h-12.8c0-4.2-1-7.3-3-9.4c-2-2.1-5.2-3.1-9.4-3.1c-3.9,0-7,1.6-9.2,4.7c-2.2,3.2-3.3,7.3-3.3,12.3v13.1
		c0,5,1.2,9.1,3.5,12.3c2.3,3.2,5.6,4.8,9.7,4.8c3.9,0,6.9-1,8.8-3.1c1.9-2.1,2.9-5.2,2.9-9.5H1233.9z"/>
	<path class="st0" d="M1280.8-31.4h-24.3v18.7h28.9v10.1h-42v-65.5h41.9v10.1h-28.8v16.5h24.3V-31.4z"/>
	<path class="st0" d="M1318.6-40.9c4.1,0,7.2-0.8,9.1-2.4c2-1.6,3-3.9,3-6.8c0-2.5-1.1-4.6-3.3-6.3c-2.2-1.7-5.4-2.5-9.5-2.5
		c-3.3,0-6,0.8-8.1,2.4c-2.2,1.6-3.2,3.6-3.2,6.1h-12.7l0-0.3c-0.2-5.4,2.1-9.8,6.8-13.2c4.7-3.4,10.5-5.1,17.4-5.1
		c8,0,14.3,1.6,18.9,4.9c4.6,3.2,7,7.9,7,13.9c0,3-1,5.7-2.9,8.2c-2,2.5-4.6,4.4-8.1,5.8c3.9,1.3,6.8,3.2,8.9,5.8
		c2.1,2.6,3.1,5.7,3.1,9.5c0,6-2.5,10.8-7.5,14.2c-5,3.4-11.5,5.1-19.5,5.1c-6.9,0-12.9-1.6-17.9-4.8c-5-3.2-7.4-7.9-7.2-14l0-0.3
		h12.8c0,2.6,1.1,4.7,3.4,6.4c2.3,1.7,5.2,2.6,8.8,2.6c4.3,0,7.6-0.9,10.1-2.6s3.7-4,3.7-6.7c0-3.4-1.1-6-3.3-7.6
		c-2.2-1.6-5.5-2.4-9.8-2.4h-8.9v-9.9H1318.6z"/>
	<path class="st0" d="M1408.1-29.4c0,8.1-2.5,14.7-7.6,20c-5.1,5.2-11.7,7.9-19.8,7.9c-8.1,0-14.7-2.6-19.7-7.9
		c-5-5.2-7.6-11.9-7.6-19.9v-11.8c0-8,2.5-14.6,7.5-19.9c5-5.3,11.6-7.9,19.6-7.9c8.1,0,14.7,2.6,19.8,7.9
		c5.1,5.3,7.6,11.9,7.6,19.9V-29.4z M1395-41.3c0-5.1-1.3-9.3-3.8-12.6s-6.1-4.9-10.6-4.9c-4.4,0-7.9,1.6-10.4,4.9
		c-2.5,3.3-3.7,7.5-3.7,12.6v11.9c0,5.2,1.2,9.4,3.7,12.7s6,4.9,10.4,4.9c4.5,0,8-1.6,10.5-4.9c2.5-3.3,3.8-7.5,3.8-12.7V-41.3z"/>
	<path class="st0" d="M1471.5-2.6h-13.1v-27.2h-26.7v27.2h-13.1v-65.5h13.1v28.2h26.7v-28.2h13.1V-2.6z"/>
</g>
<g>
	<path class="st0" d="M797.4,145.1l0.9,3.2l0.2,0l11.2-26.9H821l-19.4,43.1c-1.5,3.4-3.4,6.1-5.6,8c-2.2,1.9-5.2,2.8-9.1,2.8
		c-1.5,0-2.8-0.1-3.9-0.4c-1.1-0.3-2.5-0.6-3.9-1.1l1.2-7.6c1.1,0.3,2.1,0.6,2.9,0.7c0.8,0.2,1.7,0.2,2.5,0.2c2,0,3.5-0.4,4.6-1.3
		c1.1-0.9,1.9-2.1,2.5-3.9l0.8-2L775,121.5h11.5L797.4,145.1z"/>
</g>
</svg>'>
            </div>
        </div>
        <canvas class="draw"></canvas>
    </div>
    <div class="container container-section1">
        <div class="text">
            Бронюй проживання on-line з 3 по 11 березня та вигравай безкоштовний вікенд у квітні
        </div>
        <a href="http://reserve.bukovel.com/" class="btn" target="_blank">Бронювати</a>
    </div>
    <div class="langs">
      <a class="lang" href="ru" target="_blank" data-lang="ru">ru</a>
      <a class="lang" href="en" target="_blank" data-lang="en">en</a>
    </div>
    <div class="grider"></div>
    </div>
</div>
<div class="section section-section2" id="section2">
    <div class="container container-section2">
        <div class="title">Бронюй проживання зі знижкою до 30%</div>
        <div class="slider-block">
            <div class="slider-block__slide">
                <div class="slider-block__img">
                   <img src="img/slide.jpg" alt="Буковель"/>
                </div>
                <div class="slider-block__text">
                    <strong>Буковель</strong>
                    <p>Затишні дерев’яні будиночки у самому серці курорту. Одягай лижі на порозі і відразу спускайся з гори</p>
                    <a href="http://reserve.bukovel.com/bukovel/" class="btn" target="_blank">бронювати</a>
                </div>
            </div>
            <div class="slider-block__slide">
                <div class="slider-block__img">
                   <img src="img/slide1.jpg" alt="SHELTER"/>
                </div>
                <div class="slider-block__text">
                    <strong>SHELTER</strong>
                    <p>Готель, що знаходиться безпосередньо біля витягів № 2 і 2R. Двадцять два затишних номери створено для того, щоб кожен міг повністю відпочити після гірських розваг</p>
                    <a href="http://reserve.bukovel.com/shelter/" class="btn" target="_blank">бронювати</a>
                </div>
            </div>
            <div class="slider-block__slide">
                <div class="slider-block__img">
                   <img src="img/slide2.jpg" alt="VIP-резиденція"/>
                </div>
                <div class="slider-block__text">
                    <strong>VIP-резиденція</strong>
                    <p>Апартаменти класу «люкс» з високоякісним обслуговуванням та чудовим краєвидом для тих, хто любить відпочивати без обмежень</p>
                    <a href="http://reserve.bukovel.com/vip-residence/" class="btn" target="_blank">бронювати</a>
                </div>
            </div>
            <div class="slider-block__slide">
                <div class="slider-block__img">
                   <img src="img/slide4.jpg" alt="Ирис"/>
                </div>
                <div class="slider-block__text">
                    <strong>Ирис</strong>
                    <p>Новий комфортабельний готель знаходиться поруч із витягом №5. Класичий інтер’єр в австрійському стилі забезпечать справжній гірськолижний відпочинок</p>
                    <a href="http://reserve.bukovel.com/iris/" class="btn" target="_blank">бронювати</a>
                </div>
            </div>
            <div class="slider-block__slide">
                <div class="slider-block__img">
                   <img src="img/slide3.jpg" alt="Тавель"/>
                </div>
                <div class="slider-block__text">
                    <strong>Тавель</strong>
                    <p>Затишний готель недалеко від схилу номер 7 – біля найпопулярнішого витягу. Просторі номери зі всіма зручностями створені для повного релаксу після активного відпочинку</p>
                    <a href="http://reserve.bukovel.com/tavel/" class="btn" target="_blank">бронювати</a>
                </div>
            </div>
            <div class="slider-block__slide">
                <div class="slider-block__img">
                   <img src="img/slide5.jpg" alt="Едельвейс"/>
                </div>
                <div class="slider-block__text">
                    <strong>Едельвейс</strong>
                    <p>Невеличкий гірський готель в Яремчі з камнної залою та власним рестораном. Для гостей пропонується безкоштовний трансфер до витягів Буковель</p>
                    <a href="http://reserve.bukovel.com/edelveis/" class="btn" target="_blank">бронювати</a>
                </div>
            </div>
        </div>
        <a href="http://reserve.bukovel.com/" class="btn" target="_blank">Бронювати</a>
        <div class="text text_fullwidth">
            Деталі акції. У розіграші безкоштовного вікенду на двох у квітні автоматично беруть участь усі, хто у період з 3 по 11 березня забронював проживання в Буковелі. Переможця буде обрано за допомогою
            <a href="https://www.random.org/" target="_blank">random.org</a> 14го березня. Дізнавайся про результати розіграшу у наших соціальних мережах.
            <a href="http://facebook.com/bukovel" class="social-link" target="_blank"><i class="zmdi zmdi-facebook"></i></a>
            <a href="http://vk.com/bukovel" class="social-link" target="_blank"><i class="zmdi zmdi-vk"></i></a>
        </div>
    </div>
</div>
<div class="section section-section3" id="section3">
    <div id="ytplayer"></div>
    <div class="overlay">
        <div class="container container-section3 vertical-center">
            <div class="slogan2">Катай від 350 грн/день</div>
            <a href="https://bukovel.com/discounts/417#" class="btn" target="_blank">Купити скіпас</a>
        </div>
    <div class="grider"></div>
    </div>
</div>
<div class="footer section section-section4">
    <div class="footer__line footer__line__first">Будемо раді відповісти на додаткове запитання:</div>
    <div class="footer__line">
        <a href="http://bukovel.com" class="footer__link" target="_blank"><i class="zmdi zmdi-link"></i> bukovel.com</a><!--
           --><a href="http://facebook.com/bukovel" class="footer__link" target="_blank"><i class="zmdi zmdi-facebook"></i> facebook.com/bukovel</a><!--
           --><a href="javascript:void(0)" class="footer__link" target="_blank"><i class="zmdi zmdi-phone"></i> <span id="istat_0">+380(342)544950</span></a>
    </div>
</div>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<script charset="utf-8" src="//istat24.com.ua/js/replace.js"></script>
<script src="js/script.js?v=1" async=""></script>
</body>
</html>